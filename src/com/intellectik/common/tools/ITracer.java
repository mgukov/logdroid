package com.intellectik.common.tools;

/**
 * Created by Michael on 06.12.2015.
 */
public interface ITracer {

    void log(Trace.ELevel level, String msg, Throwable e);
}
