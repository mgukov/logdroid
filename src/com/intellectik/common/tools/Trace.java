package com.intellectik.common.tools;

/**
 * Created by mgukov on 08.09.15.
 */
public final class Trace {

    public enum ELevel {
        NONE (0, ""), ERROR (1, "ER"), WARNING (2, "W"), EVENT (4, "EV"), DEBUG (8, "D"), INFO (16, "I"), ALL (1 | 2 | 4 | 8 | 16, "");

        private int value;



        private String shortName;
        private ELevel(int value, String shortName) {
            this.value = value;
            this.shortName = shortName;
        }

        public String getShortName() {
            return shortName;
        }

        public int getValue() {
            return value;
        }

        public boolean accept(int level) {
            return (level & value) != 0;
        }
    }

    public static void setTracer(ITracer tracer) {
        Trace.tracer = tracer;
    }
    private static ITracer tracer;

    public static int getTraceLevel() {
        return traceLevel;
    }

    public static void setTraceLevel(int traceLevel) {
        Trace.traceLevel = traceLevel;
    }

    private static int traceLevel = ELevel.ALL.getValue();

    static {
        setTracer(new DefaultTracer());
    }


    private static boolean accept(ELevel level) {
        return (traceLevel & level.getValue()) != 0;
    }

    private static final void log(ELevel level, String msg, Throwable e) {
        if (accept(level) && tracer != null) {
            synchronized (tracer) {
                tracer.log(level, msg, e);
            }
        }
    }

    public static void error(String msg) {
        error(msg, null);
    }

    public static void error(Throwable e) {
        error("", e);
    }

    public static void error(String msg, Throwable e) {
        log(ELevel.ERROR, msg, e);
    }

    public static void warning(String msg) {
        log(ELevel.WARNING, msg, null);
    }

    public static void warning(String msg, Throwable e) {
        log(ELevel.WARNING, msg, e);
    }

    public static void event(String msg) {
        log(ELevel.EVENT, msg, null);
    }

    public static void debug(String msg) {
        log(ELevel.DEBUG, msg, null);
    }

    public static void info(String msg) {
        log(ELevel.INFO, msg, null);
    }
}
