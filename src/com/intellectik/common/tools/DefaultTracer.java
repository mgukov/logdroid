package com.intellectik.common.tools;

/**
 * Created by Michael on 06.12.2015.
 */
public class DefaultTracer implements ITracer {
    @Override
    public void log(Trace.ELevel level, String msg, Throwable e) {
        System.out.println(String.format("%s %s", level.getShortName(), msg));

        if (e != null) {
            System.out.println(e.getMessage());
            for (StackTraceElement element : e.getStackTrace()) {
                System.out.println(element);
            }
        }
    }
}
