package com.intellectik.logdroid;

/**
 * Project: LogdroidViewer
 * User: Michael
 * Date: 26.12.2015 16:20
 */
public interface IDeviceConnectionListener {

    void deviceConnected(DeviceInfo device);

    void deviceDisconnected(DeviceInfo device);
}
