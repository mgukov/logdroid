package com.intellectik.logdroid;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.intellectik.common.tools.Trace;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: com.intellectik.logdroid.ui.LogdroidViewer
 * User: Michael
 * Date: 26.12.2015 14:08
 */
public final class Logdroid {
    private static Logdroid instance;

    public static Logdroid getInstance() {
        if (instance == null) {
            instance = new Logdroid();
        }

        return instance;
    }

    private final static class DeviceChangeListener implements AndroidDebugBridge.IDeviceChangeListener {

        private final Logdroid tools;

        public DeviceChangeListener(Logdroid tools) {
            this.tools = tools;
        }

        @Override
        public void deviceConnected(IDevice device) {
            tools.connectDevice(device);
            Trace.debug("Device connected: " + device.getName());
        }

        @Override
        public void deviceDisconnected(IDevice device) {
            tools.disconnectDevice(device);
            Trace.debug("Device disconnected: " + device.getName());
        }

        @Override
        public void deviceChanged(IDevice device, int i) {
            Trace.debug("Device changed: " + device.getName());
        }
    }

    private boolean connected;
    private final DeviceChangeListener deviceChangeListener;
    private final List<DeviceInfo> devices;

    private final List<IDeviceConnectionListener> listeners;

    private Logdroid() {
        deviceChangeListener = new DeviceChangeListener(this);
        devices = new ArrayList<>();

        listeners = new ArrayList<>();
    }

    private void connectDevice(IDevice device) {
        DeviceInfo deviceInfo = new DeviceInfo(device);
        synchronized (devices) {
            devices.add(deviceInfo);
        }
        fireDeviceConnected(deviceInfo);
    }

    private void disconnectDevice(IDevice device) {
        DeviceInfo deviceInfo = findDevice(device.getSerialNumber());
        if (deviceInfo != null) {
            synchronized (devices) {
                devices.remove(deviceInfo);
            }

            fireDeviceDisconnected(deviceInfo);
        }
    }

    private DeviceInfo findDevice(String deviceId) {
        for (final DeviceInfo device : getConnectedDevices()) {
            if (device.getSerialNumber().equals(deviceId)) {
                return device;
            }
        }

        return null;
    }

    private void fireDeviceConnected(DeviceInfo device) {

        for (final IDeviceConnectionListener listener : new ArrayList<>(listeners)) {
            listener.deviceConnected(device);
        }
    }

    private void fireDeviceDisconnected(DeviceInfo device) {
        for (final IDeviceConnectionListener listener : new ArrayList<>(listeners)) {
            listener.deviceDisconnected(device);
        }
    }


    ////////////////////////////////
    ////////// public API //////////
    ////////////////////////////////

    public boolean addDeviceConnectionListener(IDeviceConnectionListener listener) {
        return listeners.add(listener);
    }

    public boolean removeDeviceConnectionListener(IDeviceConnectionListener listener) {
        return listeners.remove(listener);
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean connect() {
        if (connected) {
            return false;
        }

        AndroidDebugBridge.initIfNeeded(false);
        AndroidDebugBridge.addDeviceChangeListener(deviceChangeListener);
        AndroidDebugBridge.createBridge();

        connected = true;
        return true;
    }

    public void disconnect() {

        AndroidDebugBridge.removeDeviceChangeListener(deviceChangeListener);
        AndroidDebugBridge.disconnectBridge();
        AndroidDebugBridge.terminate();

        connected = false;
    }

    public DeviceInfo[] getConnectedDevices() {
        synchronized (devices) {
            return devices.toArray(new DeviceInfo[0]);
        }
    }

    public void executeShellCommand() {
    }

    ////////////////////////////////
    ////////////////////////////////
    ////////////////////////////////

}
