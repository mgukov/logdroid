package com.intellectik.logdroid;

/**
 * Project: logdroid
 * User: Michael
 * Date: 24.01.2016 16:57
 */
public class Version {
    public static final String Current = "1.0";

    private Version() {}
}
