package com.intellectik.logdroid.ui;

import com.intellectik.common.tools.Trace;
import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.Objects;

/**
 * Project: logdroid
 * User: Michael
 * Date: 17.01.2016 21:22
 */
@XmlRootElement
public class Filters {
    public final static String FILTERS_PATH = Settings.ROOT_PATH + "/filters.xml";

    private static File getFilterFile() {
        final File userDir = new File(System.getProperty("user.home"));
        final File file = new File(userDir, FILTERS_PATH);

        return file;
    }

    private static Filters createDefault() {
        final Filters filters = new Filters();
        filters.setFilters(new LogFilter[] { new LogFilter() });

        return filters;
    }

    public static Filters loadSettings() {
        try {
            final File file = getFilterFile();
            if (!file.exists()) {
                Trace.warning("settings.xml not found");
                return Filters.createDefault();
            }

            final JAXBContext jaxbContext = JAXBContext.newInstance(Filters.class);
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (Filters) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            Trace.error("Error while loading settings.xml", e);
        }

        return Filters.createDefault();
    }


    private LogFilter[] filters;
    private String currentFilterName;

    public LogFilter[] getFilters() {
        return filters;
    }

    @XmlElement
    public void setFilters(LogFilter[] filters) {
        this.filters = filters;
    }

    public String getCurrentFilterName() {
        return currentFilterName;
    }

    @XmlElement
    public void setCurrentFilterName(String name) {
        this.currentFilterName = name;
    }

    public LogFilter getCurrentFilter() {
        if (getFilters() == null || getFilters().length == 0) {
            return null;
        }

        if (getCurrentFilterName() == null || getCurrentFilterName().isEmpty()) {
            return getFilters()[0];
        }

        for(final LogFilter filter : getFilters()) {
            if (Objects.equals(getCurrentFilter(), filter.getName())) {
                return filter;
            }
        }

        return getFilters()[0];
    }

    public void save() {
        try {
            final File file = getFilterFile();
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            final JAXBContext jaxbContext = JAXBContext.newInstance(Filters.class);
            final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(this, file);
        } catch (JAXBException e) {
            Trace.error(e);
        }
    }
}
