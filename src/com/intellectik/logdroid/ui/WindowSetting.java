package com.intellectik.logdroid.ui;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Project: logdroid
 * User: Michael
 * Date: 17.01.2016 11:36
 */
@XmlType
public class WindowSetting {

    private int width;
    private int height;

    private int x;
    private int y;


    public int getWidth() { return width; }

    @XmlElement
    public void setWidth(int width) { this.width = width; }


    public int getHeight() { return height; }

    @XmlElement
    public void setHeight(int height) { this.height = height; }


    public int getX() { return x; }

    @XmlElement
    public void setX(int x) { this.x = x; }


    public int getY() { return y; }

    @XmlElement
    public void setY(int y) { this.y = y; }
}
