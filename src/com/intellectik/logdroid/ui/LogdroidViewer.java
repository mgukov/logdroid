package com.intellectik.logdroid.ui;

import com.android.ddmlib.logcat.LogCatListener;
import com.android.ddmlib.logcat.LogCatMessage;
import com.intellectik.common.tools.Trace;
import com.intellectik.logdroid.DeviceInfo;
import com.intellectik.logdroid.IDeviceConnectionListener;
import com.intellectik.logdroid.Logdroid;
import com.intellectik.logdroid.Version;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Objects;
import java.util.concurrent.Executors;

/**
 * Project: com.intellectik.android.logdroid.ui.LogdroidViewer
 * User: Michael
 * Date: 25.12.2015 23:47
 */
public final class LogdroidViewer extends JFrame {

    private DeviceInfo currentDeviceInfo;
    private final Logdroid logdroid;

    private Filters filters;
    private LogFilter currentfilter;


    private JTextPane logPane;
    private JButton startStopButton;
    private JPanel contentPanel;

    private LogCatListener logCatListener = list -> {
        for (final LogCatMessage message : list) {
            if (LogdroidViewer.this.currentfilter == null || LogdroidViewer.this.currentfilter.getLogCatFilter().matches(message)) {
                writeMessage(message);
            }
        }
    };

    public LogdroidViewer() throws HeadlessException {

        logdroid = Logdroid.getInstance();
        logdroid.connect();

        setTitle("Logdroid " + Version.Current);

        setLayout(new BorderLayout());
        contentPanel = new JPanel(new BorderLayout());
        add(contentPanel, BorderLayout.CENTER);


        createMenu();

        createToolBar();

        createLogView();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                updateWindowSettings();
            }
        });

        Logdroid.getInstance().addDeviceConnectionListener(new IDeviceConnectionListener() {
            @Override
            public void deviceConnected(DeviceInfo device) {
                // do nothing
            }

            @Override
            public void deviceDisconnected(DeviceInfo device) {
                if (currentDeviceInfo != null && Objects.equals(currentDeviceInfo.getSerialNumber(), device.getSerialNumber())) {
                    if (currentDeviceInfo.isListening()) {
                        stopListening();
                    }
                    currentDeviceInfo = null;
                }
            }
        });
    }

    private void createMenu() {
        final JMenuBar menu = new JMenuBar();
        setJMenuBar(menu);

        final JMenu menuFile = new JMenu();
        menuFile.setText("File");
        menu.add(menuFile);

        final JMenu menuHelp = new JMenu();
        menuHelp.setText("Help");
        menu.add(menuHelp);

        final JMenuItem aboutItem = menuHelp.add("About");
        aboutItem.addActionListener(e -> {
            JOptionPane.showMessageDialog(aboutItem, "Logdroid " + Version.Current, "About", JOptionPane.INFORMATION_MESSAGE);
        });

    }

    private void createToolBar() {
        final JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.setMargin(new Insets(4, 4, 4, 4));

        final JLabel label = new JLabel();
        label.setText("Device ");
        toolBar.add(label);

        final JComboBox<DeviceInfo> devicesCmb = new JComboBox<>();
        devicesCmb.addItemListener(e -> setCurrentDeviceInfo((DeviceInfo) e.getItem()));
        devicesCmb.setModel(new DevicesComboBoxModel());
        toolBar.add(devicesCmb);

        toolBar.addSeparator();

        startStopButton = new JButton();
        final ImageIcon playImage = ResourceManager.loadDefaultSizeImage(ResourceManager.PLAY_IMAGE);
        startStopButton.setIcon(playImage);
        startStopButton.addActionListener(e -> {
            if (currentDeviceInfo == null) {
                return;
            }

            if (!currentDeviceInfo.isListening()) {
                startListening();
            } else {
                stopListening();
            }
        });
        toolBar.add(startStopButton);

        final JButton clearButton = new JButton();
        final ImageIcon clearImage = ResourceManager.loadDefaultSizeImage(ResourceManager.CLEAR_IMAGE);
        clearButton.setIcon(clearImage);
        clearButton.addActionListener(e -> clearLogs());
        toolBar.add(clearButton);

        final JButton filterButton = new JButton();
        final ImageIcon filterImage = ResourceManager.loadDefaultSizeImage(ResourceManager.CONFIG_IMAGE);
        filterButton.setIcon(filterImage);
        filterButton.addActionListener(e -> openFilters());
        toolBar.add(filterButton);

        add(toolBar, BorderLayout.PAGE_START);

        setCurrentDeviceInfo((DeviceInfo) devicesCmb.getSelectedItem());
    }

    private void createLogView() {
        contentPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

        logPane = new JTextPane();
        logPane.setEditable(false);
        logPane.setFont(new Font("Consolas", Font.PLAIN, 12));//UIManager.getDefaults().getFont("Consolas")

        final JPanel logPanePanel = new JPanel(new BorderLayout());
        logPanePanel.add(logPane, BorderLayout.CENTER);

        final JScrollPane scrollPane = new JScrollPane(logPanePanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(20);
        contentPanel.add(scrollPane, BorderLayout.CENTER);

        final SearchPanel searchPanel = new SearchPanel(logPane);
        contentPanel.add(searchPanel, BorderLayout.PAGE_END);
    }

    private void openFilters() {
        final FilterPanel panel = new FilterPanel(filters);
        panel.showDialog(LogdroidViewer.this);
    }

    private void setCurrentDeviceInfo(DeviceInfo deviceInfo) {
        if (currentDeviceInfo != null && currentDeviceInfo != deviceInfo && currentDeviceInfo.isListening()) {
            currentDeviceInfo.stopListening();
            currentDeviceInfo.removeLogListener(logCatListener);
        }

        currentDeviceInfo = deviceInfo;
        if (deviceInfo != null) {
            deviceInfo.addLogListener(logCatListener);
        }
    }


    private void startListening() {
        clearLogs();
        Executors.newSingleThreadExecutor().execute(() -> {
            if (currentDeviceInfo != null) {
                updatePlayButton(true);
                currentDeviceInfo.startListening();
            }
        });
    }

    private void stopListening() {
        if (currentDeviceInfo != null) {
            currentDeviceInfo.stopListening();
        }
        updatePlayButton(false);
    }


    private void updatePlayButton(boolean play) {
        SwingUtilities.invokeLater(() -> {
            if (play) {
                final ImageIcon stopImage = ResourceManager.loadDefaultSizeImage(ResourceManager.STOP_IMAGE);
                startStopButton.setIcon(stopImage);
            } else {
                final ImageIcon playImage = ResourceManager.loadDefaultSizeImage(ResourceManager.PLAY_IMAGE);
                startStopButton.setIcon(playImage);
            }
        });
    }

    private void clearLogs() {
        SwingUtilities.invokeLater(() -> logPane.setText(""));
    }

    private void writeMessage(final LogCatMessage message) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                final StyledDocument doc = logPane.getStyledDocument();
                final SimpleAttributeSet keyWord = new SimpleAttributeSet();

                switch (message.getLogLevel()) {
                    case ERROR:
                        StyleConstants.setForeground(keyWord, Color.RED);
                        break;
                    case WARN:
                        StyleConstants.setForeground(keyWord, Color.decode("#007F0E"));
                        break;
                    case INFO:
                        StyleConstants.setForeground(keyWord, Color.BLUE);
                        break;
                    case ASSERT:
                        StyleConstants.setItalic(keyWord, true);
                        StyleConstants.setForeground(keyWord, Color.RED);
                        break;
                    case DEBUG:
                        StyleConstants.setForeground(keyWord, Color.BLACK);
                        break;
                }

                try {
                    doc.insertString(doc.getLength(), message.toString() + System.lineSeparator(), keyWord);
                } catch (Exception e) {
                    Trace.error("", e);
                }
            }
        });
    }

    private void loadFilters() {
        filters = Filters.loadSettings();
        currentfilter = filters.getCurrentFilter();
    }

    private void loadSettings() {
        final Settings settings = Settings.getInstance();
        final WindowSetting mainWindow = settings.getMainWindow();

        setSize(mainWindow.getWidth(), mainWindow.getHeight());
        setLocation(mainWindow.getX(), mainWindow.getY());
    }

    private void updateWindowSettings() {
        final Settings settings = Settings.getInstance();

        final WindowSetting mainWindow = settings.getMainWindow();
        mainWindow.setWidth(getWidth());
        mainWindow.setHeight(getHeight());
        mainWindow.setX(getLocation().x);
        mainWindow.setY(getLocation().y);

        settings.save();
    }


    public static void main(String[] args) {

        final LogdroidViewer logdroid = new LogdroidViewer();
        logdroid.loadSettings();
        logdroid.loadFilters();

        logdroid.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        logdroid.setVisible(true);
    }
}
