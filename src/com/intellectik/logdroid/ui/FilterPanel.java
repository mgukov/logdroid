package com.intellectik.logdroid.ui;

import com.android.ddmlib.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Project: Logdroid
 * User: Michael
 * Date: 26.12.2015 22:00
 */
public class FilterPanel extends JPanel {

    private JDialog ownDialog;
    private Filters filters;

    private final JTextField txtName;
    private final JComboBox<Log.LogLevel> cmbLogLevel;
    private final JTextField txtPackageName;
    private final JTextField txtTag;
    private final JTextField txtText;

    public FilterPanel(Filters filters) {
        this.filters = filters;

        final LogFilter filter = filters.getCurrentFilter();

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEmptyBorder(10, 8, 10, 8));

        final JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new GridBagLayout());
        contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));

        // name
        final JLabel lblName = new JLabel("Filter name:");
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(0, 0, 0, 5);
        c.anchor =  GridBagConstraints.LINE_END;
        contentPanel.add(lblName, c);

        txtName = new JTextField();
        if (filter != null) {
            txtName.setText(filter.getName());
        }
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor =  GridBagConstraints.LINE_START;
        contentPanel.add(txtName, c);


        // log level
        final JLabel lblLogLevel = new JLabel("Log level:");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor =  GridBagConstraints.LINE_END;
        contentPanel.add(lblLogLevel, c);


        cmbLogLevel = new JComboBox<>();
        cmbLogLevel.setModel(new DefaultComboBoxModel<>(Log.LogLevel.values()));

        if (filter != null && filter.getLogLevel() != null && !filter.getLogLevel().isEmpty()) {
            cmbLogLevel.setSelectedItem(Log.LogLevel.getByLetterString(filter.getLogLevel()));
        }
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor =  GridBagConstraints.LINE_START;
        c.insets = new Insets(5, 0, 0, 0);
        contentPanel.add(cmbLogLevel, c);


        // package name
        final JLabel lblPackageName = new JLabel("Package name:");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 2;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor =  GridBagConstraints.LINE_END;
        contentPanel.add(lblPackageName, c);


        txtPackageName = new JTextField();
        if (filter != null) {
            txtPackageName.setText(filter.getAppName());
        }
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor =  GridBagConstraints.LINE_START;
        c.insets = new Insets(5, 0, 0, 0);
        contentPanel.add(txtPackageName, c);


        // tag
        final JLabel lblTag = new JLabel("Tag:");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 3;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor =  GridBagConstraints.LINE_END;
        contentPanel.add(lblTag, c);


        txtTag = new JTextField();
        if (filter != null) {
            txtTag.setText(filter.getTag());
        }
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor =  GridBagConstraints.LINE_START;
        c.insets = new Insets(5, 0, 0, 0);
        contentPanel.add(txtTag, c);

        // text
        final JLabel lblText = new JLabel("Text:");
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 4;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor =  GridBagConstraints.LINE_END;
        contentPanel.add(lblText, c);


        txtText = new JTextField();
        if (filter != null) {
            txtText.setText(filter.getText());
        }
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 4;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor =  GridBagConstraints.LINE_START;
        c.insets = new Insets(5, 0, 0, 0);
        contentPanel.add(txtText, c);


        // content
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weightx = 1;
        c.fill = GridBagConstraints.BOTH;
        add(contentPanel, c);

        final JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(e -> cancelChanges());
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor =  GridBagConstraints.PAGE_END;
        add(btnCancel, c);

        final JButton btnOk = new JButton("OK");
        btnOk.addActionListener(e -> applyChanges());
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor =  GridBagConstraints.PAGE_END;
        add(btnOk, c);
    }

    public void showDialog(JFrame frame) {
        ownDialog = new JDialog(frame, "Filter", Dialog.ModalityType.APPLICATION_MODAL);
        ownDialog.setLayout(new BorderLayout());
        ownDialog.add(this, BorderLayout.CENTER);

        final Settings settings = Settings.getInstance();
        final WindowSetting filterWindow = settings.getFilterWindow();

        ownDialog.setSize(filterWindow.getWidth(), filterWindow.getHeight());
        ownDialog.setLocation(filterWindow.getX(), filterWindow.getY());

        ownDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        ownDialog.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentHidden(ComponentEvent e) {
                updateWindowSettings();
            }
        });
        ownDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                updateWindowSettings();
            }
        });

        ownDialog.setVisible(true);

    }

    private void updateWindowSettings() {
        final Settings settings = Settings.getInstance();

        final WindowSetting filterWindow = settings.getFilterWindow();
        filterWindow.setWidth(ownDialog.getWidth());
        filterWindow.setHeight(ownDialog.getHeight());
        filterWindow.setX(ownDialog.getLocation().x);
        filterWindow.setY(ownDialog.getLocation().y);

        settings.save();
    }

    private void cancelChanges() {
        ownDialog.setVisible(false);
    }

    private void applyChanges() {

        final LogFilter filter = filters.getCurrentFilter();
        if (filter == null) {
            return;
        }

        filter.setAppName(txtPackageName.getText());
        filter.setLogLevel(((Log.LogLevel)cmbLogLevel.getSelectedItem()).name());
        filter.setName(txtName.getText());
        filter.setTag(txtTag.getText());
        filter.setText(txtText.getText());
        filter.updateFilter();

        filters.save();

        ownDialog.setVisible(false);
    }
}
