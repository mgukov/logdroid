package com.intellectik.logdroid.ui;

import com.intellectik.logdroid.Logdroid;
import com.intellectik.logdroid.DeviceInfo;
import com.intellectik.logdroid.IDeviceConnectionListener;

import javax.swing.*;

/**
 * Project: Logdroid
 * User: Michael
 * Date: 26.12.2015 17:24
 */
final class DevicesComboBoxModel extends DefaultComboBoxModel<DeviceInfo> {

    public DevicesComboBoxModel() {
        super();

        final Logdroid tools = Logdroid.getInstance();

        for (final DeviceInfo info : tools.getConnectedDevices()) {
            addElement(info);
        }

        tools.addDeviceConnectionListener(new IDeviceConnectionListener() {
            @Override
            public void deviceConnected(DeviceInfo device) {
                addElement(device);
                DevicesComboBoxModel.this.fireIntervalAdded(
                        DevicesComboBoxModel.this, DevicesComboBoxModel.this.getSize() - 1, DevicesComboBoxModel.this.getSize() - 1);
            }

            @Override
            public void deviceDisconnected(DeviceInfo device) {
                removeElement(device);
                DevicesComboBoxModel.this.fireIntervalRemoved(
                        DevicesComboBoxModel.this, DevicesComboBoxModel.this.getSize(), DevicesComboBoxModel.this.getSize());
            }
        });
    }
}
