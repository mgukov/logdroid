package com.intellectik.logdroid.ui;

import com.intellectik.common.tools.Trace;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * Project: Logdroid
 * User: Michael
 * Date: 16.01.2016 22:28
 */
public final class ResourceManager {
    public final static String PLAY_IMAGE = "/play.png";
    public final static String STOP_IMAGE = "/stop.png";
    public final static String CLEAR_IMAGE = "/clear.png";
    public final static String CONFIG_IMAGE = "/config.png";

    public final static int DEFAULT_SIZE = 15;

    private ResourceManager() {}

    public static ImageIcon loadImage(String image) {
        final InputStream play = ResourceManager.class.getResourceAsStream(image);
        try {
            byte[] playBytes = IOUtils.toByteArray(play);
            return new ImageIcon(playBytes);
        } catch (IOException e) {
            Trace.error(e);
            return null;
        }
    }

    public static ImageIcon loadImage(String image, int width, int height) {
        final InputStream play = ResourceManager.class.getResourceAsStream(image);

        final ImageIcon icon = loadImage(image);
        if (icon != null) {
            Image scaled = icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
            return new ImageIcon(scaled);
        }
        return null;
    }

    public static ImageIcon loadDefaultSizeImage(String image) {
        return loadImage(image, DEFAULT_SIZE, DEFAULT_SIZE);
    }
}
