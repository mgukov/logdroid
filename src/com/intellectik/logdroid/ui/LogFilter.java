package com.intellectik.logdroid.ui;

import com.android.ddmlib.Log;
import com.android.ddmlib.logcat.LogCatFilter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Project: Logdroid
 * User: Michael
 * Date: 05.01.2016 15:21
 */

@XmlType
public class LogFilter {

    private String name;
    private String tag;
    private String text;
    private String pid;
    private String appName;
    private String logLevel;

    private LogCatFilter logCatFilter;

    public LogFilter() {
        logLevel = Log.LogLevel.VERBOSE.name();
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    @XmlElement
    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getText() {
        return text;
    }

    @XmlElement
    public void setText(String text) {
        this.text = text;
    }

    public String getPid() {
        return pid;
    }

    @XmlElement
    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAppName() {
        return appName;
    }

    @XmlElement
    public void setAppName(String appName) {
        this.appName = appName;
    }


    public String getLogLevel() {
        return logLevel;
    }

    @XmlElement
    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public void updateFilter() {
        logCatFilter = new LogCatFilter(getValue(name), getValue(tag), getValue(text), getValue(pid), getValue(appName), Log.LogLevel.getByLetterString(logLevel));
    }

    private String getValue(String value) {
        return value != null ? value : "";
    }

    public LogCatFilter getLogCatFilter() {
        if (logCatFilter == null) {
            updateFilter();
        }
        return logCatFilter;
    }


}
