package com.intellectik.logdroid.ui;

import com.intellectik.common.tools.Trace;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Project: Logdroid
 * User: Michael
 * Date: 17.01.2016 10:42
 */
@XmlRootElement
public class Settings {

    public final static String ROOT_PATH = ".logdroid";
    public final static String SETTINGS_PATH = ROOT_PATH + "/settings.xml";


    public static Settings instance;

    public static Settings getInstance() {
        if (instance == null) {
            instance = loadSettings();
        }

        return instance;
    }

    private static File getSettingsFile() {
        final File userDir = new File(System.getProperty("user.home"));
        final File file = new File(userDir, SETTINGS_PATH);
        return file;
    }


    private static Settings loadSettings() {
        try {
            final File file = getSettingsFile();
            if (!file.exists()) {
                Trace.warning("settings.xml not found");
                return Settings.createDefault();
            }

            final JAXBContext jaxbContext = JAXBContext.newInstance(Settings.class);
            final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (Settings) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            Trace.error("Error while loading settings.xml", e);
        }

        return Settings.createDefault();
    }

    private static Settings createDefault() {
        final WindowSetting main = new WindowSetting();
        main.setWidth(800);
        main.setHeight(600);
        main.setX(100);
        main.setY(100);

        final WindowSetting filter = new WindowSetting();
        filter.setWidth(400);
        filter.setHeight(270);
        filter.setX(150);
        filter.setY(150);

        final Settings settings = new Settings();
        settings.setFilterWindow(filter);
        settings.setMainWindow(main);

        settings.save();

        return settings;
    }

    private WindowSetting mainWindow;
    private WindowSetting filterWindow;

    @XmlElement
    public void setMainWindow(WindowSetting setting) {
        this.mainWindow = setting;
    }

    public WindowSetting getMainWindow() {
        return mainWindow;
    }


    @XmlElement
    public void setFilterWindow(WindowSetting setting) {
        this.filterWindow = setting;
    }


    public WindowSetting getFilterWindow() {
        return filterWindow;
    }

    public void save() {
        try {
            final File file = getSettingsFile();
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            final JAXBContext jaxbContext = JAXBContext.newInstance(Settings.class);
            final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(this, file);
        } catch (JAXBException e) {
            Trace.error(e);
        }
    }
}
