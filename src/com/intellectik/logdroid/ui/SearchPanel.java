package com.intellectik.logdroid.ui;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Project: Logdroid
 * User: Michael
 * Date: 09.01.2016 12:32
 */
public class SearchPanel extends JPanel {

    private final JTextPane textPane;
    private final JTextField txtSearch;

    private int lastSearchPosition;

    public SearchPanel(JTextPane textPane) {
        this.textPane = textPane;

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));

        txtSearch = new JTextField();
        txtSearch.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                changedUpdate(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                changedUpdate(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                resetSearch();
            }
        });

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;

        c.weightx = 1;
        c.fill = GridBagConstraints.BOTH;
        add(txtSearch, c);


        final JButton btnFind = new JButton("Find");
        btnFind.addActionListener(e -> findAndSelect());
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.insets = new Insets(0, 4, 0, 0);
        add(btnFind, c);

    }

    private void findAndSelect() {
        final String text = textPane.getText().replace("\r\n", " ");
        final String searchStr = txtSearch.getText();

        if (text .isEmpty() || searchStr.isEmpty()) {
            return;
        }

        if (lastSearchPosition > text.length()) {
            lastSearchPosition = -1;
        }

        lastSearchPosition = find(lastSearchPosition + 1, text, searchStr);
        if (lastSearchPosition == -1) {
            return;
        }

        textPane.requestFocus();
        textPane.setSelectionStart(lastSearchPosition);
        textPane.setSelectionEnd(lastSearchPosition + searchStr.length());
    }

    private int find(int start, String text, String findStr) {
        return text.indexOf(findStr, start);
    }

    public void resetSearch() {
        lastSearchPosition = -1;
    }
}
