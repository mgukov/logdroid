package com.intellectik.logdroid;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.logcat.LogCatListener;
import com.android.ddmlib.logcat.LogCatReceiverTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: LogdroidViewer
 * User: Michael
 * Date: 26.12.2015 15:58
 */
public class DeviceInfo {

    private final IDevice device;
    private LogCatReceiverTask receiverTask;

    private final List<LogCatListener> listeners;
    private final LogCatListener listener;

    DeviceInfo(IDevice device) {
        this.device = device;

        listeners = new ArrayList<>();
        listener = list -> {
             for (final LogCatListener l : listeners) {
                  l.log(list);
             }
        };
    }

    public String getSerialNumber() {
        return device.getSerialNumber();
    }

    public String getName() {
        return device.getName();
    }

    public IDevice getDevice() {
        return device;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void startListening() {
        receiverTask = new LogCatReceiverTask(device);
        receiverTask.addLogCatListener(listener);
        try {
            receiverTask.run();
        } finally {
            receiverTask.removeLogCatListener(listener);
            receiverTask = null;
        }
    }

    public void stopListening() {
        receiverTask.stop();
    }

    public void addLogListener(LogCatListener l) {
        listeners.add(l);
    }

    public void removeLogListener(LogCatListener l) {
        listeners.remove(l);
    }

    public boolean isListening() {
        return receiverTask != null;
    }
}
